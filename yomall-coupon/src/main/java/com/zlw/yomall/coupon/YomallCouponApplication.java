package com.zlw.yomall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YomallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(YomallCouponApplication.class, args);
    }

}
