package com.zlw.yomall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YomallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(YomallWareApplication.class, args);
    }

}
