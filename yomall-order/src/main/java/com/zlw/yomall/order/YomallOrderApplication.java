package com.zlw.yomall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YomallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(YomallOrderApplication.class, args);
    }

}
