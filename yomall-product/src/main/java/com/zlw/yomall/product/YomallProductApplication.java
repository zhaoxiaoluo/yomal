package com.zlw.yomall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YomallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(YomallProductApplication.class, args);
    }

}
