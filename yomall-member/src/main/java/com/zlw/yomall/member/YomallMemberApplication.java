package com.zlw.yomall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YomallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(YomallMemberApplication.class, args);
    }

}
